export default {
  actions: {
    async fetchTodos(ctx) {
      const res = await fetch('https://jsonplaceholder.typicode.com/todos?_limit=3');
      const todos = await res.json();
      console.log(todos);
      ctx.commit('updateTodos', todos);
      ctx.commit('updateLoadingStatus', false);
    }
  },
  mutations: {
    updateTodos(state, todos) {
      state.todos = todos;
    },
    updateLoadingStatus(state, status) {
      state.loadingStatus = status;
    },
    createNewTodo(state, newTodo) {
      state.todos.unshift(newTodo);
    },
    deleteTodo(state, todoId) {
      state.todos = state.todos.filter(({ id }) => id !== todoId);
    },
  },
  state: {
    todos: [],
    loadingStatus: true,
  },
  getters: {
    todos: (state) => state.todos,
    loadingStatus: (state) => state.loadingStatus,
  },
}